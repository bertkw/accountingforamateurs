# AccountingForAmateurs
Back in 2011 my employers purchased an accounting package called Accounting for Practitioners, hoping to streamline the process of producing annual financial statements for their clients.  I was tasked with incorporating this software package into our business practices, including initial setup of the AFP files and training of our employees.

Unfortunately, after much examination and experimentation we found the product to be unsuitable for our purposes.  Its concept was good but we felt its execution was poor (I will refrain from going into further detail about its perceived shortcomings; it's rude, and the software may have improved by now).  Being a programmer myself I took it upon myself to take parts of AFP that we liked and produce a software package that fit our needs.  AccountingForAmateurs (AFA) was born.

## Basic workflow
1. The AFA software consists of an executable and two template files.  
2. You fill out the template excel (.xlsm) file with your company's data and a basic skeleton of customized financial statement notes and sundry.  This portion of the file remains largely the same from year to year.
3. You export the year's trial balance from Quickbooks as an excel file.
4. You import it into the .xlsm file using the executable, carrying the previous year's data forward first (using the "Carry Forward" feature).
5. You edit and adjust the trial balance data as needed within the .xlsm file.
6. You use generate a word file from the excel file.  This word file is the year's financial statements.
7. You review and customize the word and excel files as necessary.  Using MVVM terminology, the word file is the "View" for the data, and the excel file is the "ViewModel".  You change numbers in the excel file and use the program to update them in the word file.
8. When you are satisfied with the results, you can produce a GIFI file from the excel file using the "export GIFI" feature.  
9. This GIFI file is importable into the Cantax software package as a step in the preparation of the company's annual tax return.

## Results
The AFA software package has transformed the way we produce company financial statements and tax returns.  These documents used to be typed out manually in Wordperfect, and then the figures were manually entered into Cantax T2; this was a time consuming and error-prone process, especially when multiple drafts were involved.  Each draft had to be painstakingly reviewed and re-reviewed in both Wordperfect and Cantax. 

With AFA, on the other hand, the senior associate would make edits to the company's trial balance within Quickbooks with journal entries.  These edits would be mirrored into our AFA .xlsm files and updated into the Word file and eventually the Cantax file; this cut down on typos, formatting errors, arithmetic errors, etc.  

A process that used to take a senior (expensive) employee hours of just typing, editing, and figure checking now took a non-accountant 10-15 minutes, with higher confidence in the veracity of the final numbers.

## Quick code review, 5 years later
In preparing this repository for publishing I had a chance to re-examine the code.  If you do skim or read the source code, please keep in mind that I was given only a month to dedicate myself to the task by my employer, without any previous experience in WPF or programatically manipulating Microsoft Office documents.  Please don't be too harsh :).  I ended up with a working prototype in nearly 3 weeks, and spent another few months refining it while in production.  The final changes to the program were made in September of 2013 according to some old file server timestamps.

Thoughts about the code:

1. I would try to avoid the Microsoft Office Interop libraries.  At the time, and given my time constraints, I couldn't coax the OpenOfficeXml library to do what I wanted.  Today, there are several libraries I'd consider before resorting to Microsoft Office Interop, as it's quite slow and somewhat error prone.
2. I butchered the whole MVVM concept.  The WPF code looks like a hybrid of Windows.Forms and WPF.  Maybe that's okay, maybe not.  In any case it could be much better.
3. The code needs a lot more documentation.  
4. I'm somewhat glad I had the (minimal) foresight to produce an intermediate data structure for the document (AFARenderData).  Unfortunately, the data structure is not documented at all, but its existence is a start.  Because this data structure exists, it would be relatively simple to produce output files in formats other than Word (e.g. LaTeX or postscript or PDF, for instance).
5. The program was formerly broken up into a DLL, an executable, and several small test applications.  I removed the test applications and combined the DLL and executable projects into one.
