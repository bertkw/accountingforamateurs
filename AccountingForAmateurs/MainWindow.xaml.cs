﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Diagnostics;

namespace AFA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IAFAViewModel ViewModel;
        private StringBuilder _output;

        private string _currentAFAFile;
        private string _currentDocFile;
        string CurrentAFAFile
        {
            get { return _currentAFAFile; }
            set
            {
                _currentAFAFile = value;
                Title = "Accounting for Amateurs - " + value;
                UpdateStatusBar();
            }
        }
        string CurrentFSFile
        {
            get { return _currentDocFile; }
            set
            {
                _currentDocFile = value;
                UpdateStatusBar();
            }
        }
        string BaseFilenameGuess
        {
            get
            {
                if (CurrentAFAFile == null)
                    return "afafile.afa";
                return System.IO.Path.GetFileNameWithoutExtension(CurrentAFAFile);
            }
        }
        string BaseFilenameGuessWithAFASuffix
        {
            get
            {
                string s = BaseFilenameGuess;
                if (s.EndsWith(".afa")) return s;
                return s + ".afa";
            }
        }


        private void UpdateStatusBar()
        {
            string afa = CurrentAFAFile;
            if (afa == null) afa = "none";
            string fs = CurrentFSFile;
            if (fs == null) fs = "none";
            Status.Text = "  AFA File (Excel): " + afa + "\n  FS File (Word): " + fs;
        }
        public class ErrorHandler : IAFAErrorHandler
        {
            private StringBuilder sb;
            public ErrorHandler(StringBuilder output)
            {
                sb = output;
            }
            public bool HandleException(Exception e)
            {
                sb.AppendLine(e.Message);
                sb.AppendLine(e.StackTrace);
                sb.AppendLine("");
                return false;
            }
        }

        private void AppendOutput(string s)
        {
            _output.AppendLine(s);
            OutputBox.Text = _output.ToString();
            OutputBox.ScrollToEnd();
        }
        private void AppendBlankLineOutput()
        {
            _output.Append("\n");
        }
        private void ClearOutput()
        {
            _output.Clear();
            OutputBox.Text = _output.ToString();
            OutputBox.ScrollToHome();
        }

        private bool CheckFileOpenable(string fname)
        {
            try
            {
                using (FileStream f = File.OpenWrite(fname))
                    f.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void SetDialogDirectory (Microsoft.Win32.FileDialog d)
        {
            if (CurrentAFAFile != null && CurrentAFAFile.Length > 0)
                d.InitialDirectory = System.IO.Path.GetDirectoryName(CurrentAFAFile);
        }

        public MainWindow()
        {
            InitializeComponent();
            ViewModel = AFAViewModel.GetInstance();
            _output = new StringBuilder(1024);
            ViewModel.ErrorHandler = new ErrorHandler(_output);
            CurrentFSFile = null;
            UpdateStatusBar();
        }

        private void Menu_New_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "new_afa_file.afa.xlsm"; // Default file name
            dlg.DefaultExt = ".xlsm"; // Default file extension
            dlg.Filter = "All files (.*)|*.*";
            dlg.AddExtension = true;
            dlg.OverwritePrompt = true;
            SetDialogDirectory(dlg);
            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                AppendOutput("File to create: " + dlg.FileName);
                if (File.Exists(dlg.FileName) && !CheckFileOpenable(dlg.FileName))
                {
                    AppendOutput("File exists but cannot be deleted: " + dlg.FileName);
                    AppendOutput("Make sure the file is not open in Excel.");
                }
                if (ViewModel.NewAFAFile(dlg.FileName))
                {
                    CurrentAFAFile = dlg.FileName;
                    AppendOutput(CurrentAFAFile + " successfully created!");
                }
                AppendBlankLineOutput();
            }
        }

        private void Menu_Open_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = BaseFilenameGuess + ".xlsm";
            dlg.DefaultExt = ".xlsm";
            dlg.Filter = "All files (.*)|*.*"; SetDialogDirectory(dlg);
            Nullable<bool> result = dlg.ShowDialog();
            dlg.AddExtension = true;
            dlg.CheckFileExists = true;
            if (result == false)
                return;
            AppendOutput("File to open: " + dlg.FileName);
            if (CheckFileOpenable(dlg.FileName))
            {
                CurrentAFAFile = dlg.FileName;
                AppendOutput("Successfully opened file: " + CurrentAFAFile);
            }
            else
            {
                AppendOutput("Could not open file: " + dlg.FileName);
                AppendOutput("Make sure the afa file is not open in Excel.");
            }
            AppendBlankLineOutput();
        }

        private void Menu_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Menu_Previous_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "previousyeartb.xlsx";
            dlg.DefaultExt = ".xlsx";
            dlg.Filter = "All files (.*)|*.*";
            dlg.Title = "Import Previous Year"; SetDialogDirectory(dlg);
            Nullable<bool> result = dlg.ShowDialog();
            if (result == false)
                return;
            AppendOutput("File to import previous year from: " + dlg.FileName);
            if (!CheckFileOpenable(dlg.FileName))
            {
                AppendOutput("Could not open file: " + dlg.FileName);
                AppendOutput("Make sure the tb excel file is not open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            if (!CheckFileOpenable(CurrentAFAFile))
            {
                AppendOutput("Could not open file: " + CurrentAFAFile);
                AppendOutput("Make sure the afa file is not open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            if (!ViewModel.ImportPreviousYear(CurrentAFAFile, dlg.FileName))
            {
                AppendOutput("Failed to import previous year trial balance!");
                AppendBlankLineOutput();
                return;
            }
            AppendOutput("Previous year trial balance successfully imported.");
            AppendBlankLineOutput();
        }

        private void Menu_Current_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "currentyeartb.xlsx";
            dlg.DefaultExt = ".xlsx";
            dlg.Filter = "All files (.*)|*.*";
            dlg.Title = "Import Current Year"; SetDialogDirectory(dlg);
            Nullable<bool> result = dlg.ShowDialog();
            if (result == false)
                return;
            AppendOutput("File to import current year from: " + dlg.FileName);
            if (!CheckFileOpenable(dlg.FileName))
            {
                AppendOutput("Could not open file: " + dlg.FileName);
                AppendOutput("Make sure the tb excel file is not open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            if (!CheckFileOpenable(CurrentAFAFile))
            {
                AppendOutput("Could not open file: " + CurrentAFAFile);
                AppendOutput("Make sure the afa file is not open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            if (!ViewModel.ImportCurrentYear(CurrentAFAFile, dlg.FileName))
            {
                AppendOutput("Failed to import current year trial balance!");
                AppendBlankLineOutput();
                return;
            }
            AppendOutput("Current year trial balance successfully imported.");
            AppendBlankLineOutput();
        }

        private void Menu_Word_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = BaseFilenameGuessWithAFASuffix + ".docx"; // Default file name
            dlg.DefaultExt = ".docx"; // Default file extension
            dlg.Filter = "All files (.*)|*.*";
            SetDialogDirectory(dlg);
            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == false)
                return;
            AppendOutput("Word file to create: " + dlg.FileName);
            if (!CheckFileOpenable(dlg.FileName))
            {
                AppendOutput("Could not open file: " + dlg.FileName);
                AppendOutput("Make sure the finanwcial statements file is not open in Word.");
                AppendBlankLineOutput();
                return;
            }
            if (!CheckFileOpenable(CurrentAFAFile))
            {
                AppendOutput("Could not open file: " + CurrentAFAFile);
                AppendOutput("Make sure the afa file is not open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            if (!ViewModel.CreateFinancialStatements(CurrentAFAFile, dlg.FileName))
            {
                AppendOutput("Failed to create financial statements!");
                AppendBlankLineOutput();
                return;
            }
            AppendOutput("Financial statements successfully created.");
            AppendBlankLineOutput();
            CurrentFSFile = dlg.FileName;
        }

        private void Menu_GIFI_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = BaseFilenameGuess + ".gfi"; // Default file name
            dlg.DefaultExt = ".gfi"; // Default file extension
            dlg.Filter = "All files (.*)|*.*";
            dlg.OverwritePrompt = true; SetDialogDirectory(dlg);
            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == false)
                return;
            AppendOutput("GIFI file to create: " + dlg.FileName);
            if (!CheckFileOpenable(dlg.FileName))
            {
                AppendOutput("Could not open file: " + dlg.FileName);
                AppendBlankLineOutput();
                return;
            }
            if (!CheckFileOpenable(CurrentAFAFile))
            {
                AppendOutput("Could not open file: " + CurrentAFAFile);
                AppendOutput("Make sure the afa file is not open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            if (!ViewModel.ExportGifi(CurrentAFAFile, dlg.FileName))
            {
                AppendOutput("Failed to export GIFI file!");
                AppendBlankLineOutput();
                return;
            }
            AppendOutput("GIFI successfully exported.");
            AppendBlankLineOutput();
        }

        private void Menu_Clear_Click(object sender, RoutedEventArgs e)
        {
            ClearOutput();
        }

        private void Menu_SaveAs_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = BaseFilenameGuessWithAFASuffix + ".2.xlsm";
            dlg.DefaultExt = ".xlsm";
            dlg.Filter = "All files (.*)|*.*";
            dlg.OverwritePrompt = true; SetDialogDirectory(dlg);
            Nullable<bool> result = dlg.ShowDialog();
            if (result == false)
                return;
            AppendOutput("File to save as: " + dlg.FileName);
            if (!CheckFileOpenable(dlg.FileName))
            {
                AppendOutput("Could not open file: " + dlg.FileName);
                AppendOutput("Make sure the target file is not open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            if (!CheckFileOpenable(CurrentAFAFile))
            {
                AppendOutput("Could not open file: " + CurrentAFAFile);
                AppendOutput("Make sure the source file is not open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            try
            {
                if (File.Exists(dlg.FileName))
                    File.Delete(dlg.FileName);
                File.Copy(CurrentAFAFile, dlg.FileName);
                CurrentAFAFile = dlg.FileName;
            }
            catch (Exception ex)
            {
                AppendOutput("Save as operation failed!");
                AppendBlankLineOutput();
                return;
            }
            AppendOutput("Save as successfully completed.");
            AppendBlankLineOutput();

        }

        private void Menu_CarryForward_Click(object sender, RoutedEventArgs e)
        {
            string warning = "This action will alter the contents of the selected AFA file.  If this is the original file, please consider working on a copy of the file instead.\n\nDo you wish to continue?";
            if (MessageBox.Show(warning, "Warning", MessageBoxButton.YesNo) == MessageBoxResult.No)
            {
                AppendOutput("Carry forward cancelled!");
                return;
            }
            AppendOutput("File to carry forward: " + CurrentAFAFile);
            if (!CheckFileOpenable(CurrentAFAFile))
            {
                AppendOutput("Could not open file: " + CurrentAFAFile);
                AppendOutput("Make sure the afa file is not open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            if (!ViewModel.CarryForward(CurrentAFAFile))
            {
                AppendOutput("Carry forward failed!");
                AppendBlankLineOutput();
                return;
            }
            AppendOutput("Carry forward successful.");
            AppendBlankLineOutput();
        }

        private void Menu_ViewExcel_Click(object sender, RoutedEventArgs e)
        {
            AppendOutput("File to view in Excel: " + CurrentAFAFile);
            if (!CheckFileOpenable(CurrentAFAFile))
            {
                AppendOutput("Could not open file: " + CurrentAFAFile);
                AppendOutput("The file may already be open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            Process.Start(CurrentAFAFile);
            AppendOutput("Excel successfully started.");
            AppendBlankLineOutput();
        }

        private void Menu_ViewWord_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentFSFile == null)
            {
                AppendOutput("No active financial statements file.  Please create or select one.");
                AppendBlankLineOutput();
                return;
            }
            AppendOutput("File to view in Excel: " + CurrentFSFile);
            if (!CheckFileOpenable(CurrentFSFile))
            {
                AppendOutput("Could not open file: " + CurrentFSFile);
                AppendOutput("Make sure the file is not open in Word.");
                AppendBlankLineOutput();
                return;
            }
            Process.Start(CurrentFSFile);
            AppendOutput("Word successfully started.");
            AppendBlankLineOutput();
        }

        private void Menu_SelectWord_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = BaseFilenameGuessWithAFASuffix + ".docx";
            dlg.DefaultExt = ".docx"; SetDialogDirectory(dlg);
            dlg.Filter = "All files (.*)|*.*";
            Nullable<bool> result = dlg.ShowDialog();
            dlg.AddExtension = true;
            dlg.CheckFileExists = true;
            if (result == false)
                return;
            AppendOutput("File to open: " + dlg.FileName);
            if (File.Exists(dlg.FileName))
            {
                CurrentFSFile = dlg.FileName;
                AppendOutput("Selected file: " + CurrentFSFile);
                AppendBlankLineOutput();
            }
            else
            {
                AppendOutput("Could not open file: " + dlg.FileName);
                AppendOutput("Make sure the afa file is not open in Excel.");
                AppendBlankLineOutput();
            }
        }

        private void Menu_Word_Update_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = BaseFilenameGuessWithAFASuffix + ".docx";
            dlg.DefaultExt = ".docx"; SetDialogDirectory(dlg);
            dlg.Filter = "All files (.*)|*.*";
            Nullable<bool> result = dlg.ShowDialog();
            dlg.AddExtension = true;
            dlg.CheckFileExists = true;
            if (result == false)
                return;
            AppendOutput("File to open: " + dlg.FileName);
            if (File.Exists(dlg.FileName))
            {
            }
            else
            {
                AppendOutput("Could not open file: " + dlg.FileName);
                AppendOutput("Make sure the afa file is not open in Excel.");
                AppendBlankLineOutput();
                return;
            }
            CurrentFSFile = dlg.FileName;
            if (!ViewModel.UpdateWord(CurrentAFAFile, dlg.FileName))
            {
                AppendOutput("Word Update failed!");
                AppendBlankLineOutput();
                return;
            }
            AppendOutput("Word Update successful.");
            AppendBlankLineOutput();
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            if (e.Data is System.Windows.DataObject && ((System.Windows.DataObject)e.Data).ContainsFileDropList())
            {
                foreach (string filePath in ((System.Windows.DataObject)e.Data).GetFileDropList())
                {
                    if (CheckFileOpenable(filePath))
                    {
                        string s = filePath.ToLower();
                        if (s.EndsWith(".xlsm"))
                        {
                            CurrentAFAFile = filePath;
                            AppendOutput("Successfully opened file: " + CurrentAFAFile);
                            AppendBlankLineOutput();
                            Directory.SetCurrentDirectory(System.IO.Path.GetDirectoryName(filePath));
                        }
                        else if (s.EndsWith(".docx"))
                        {
                            CurrentFSFile = filePath;
                            AppendOutput("Selected file: " + CurrentFSFile);
                            AppendBlankLineOutput();
                            Directory.SetCurrentDirectory(System.IO.Path.GetDirectoryName(filePath));
                        }
                    }
                    else
                    {
                        AppendOutput("Could not open file: " + filePath);
                        AppendOutput("Make file is not open.");
                        AppendBlankLineOutput();
                        return;
                    }
                }
            }
        }
        private void TextBox_PreviewDragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.All;
            e.Handled = true;
        }
        /*
        private void TextBox_PreviewDrop(object sender, DragEventArgs e)
        {
            object text = e.Data.GetData(DataFormats.FileDrop);
            TextBox tb = sender as TextBox;
            if (tb != null)
            {
                tb.Text = string.Format("{0}", ((string[])text)[0]);
            }
        }*/
    }
}
