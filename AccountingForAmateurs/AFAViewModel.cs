﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using AccountingForAmateurs.Model;

namespace AFA
{
    public class AFAViewModel : IAFAViewModel
    {
        private static AFAViewModel _instance;
        private string _excelTemplate;
        private string _wordtemplate;
        private IAFAErrorHandler _eh;

        private void ClearZombieCOMObjects()
        {
            System.Runtime.InteropServices.Marshal.CleanupUnusedObjectsInCurrentContext();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            System.Runtime.InteropServices.Marshal.CleanupUnusedObjectsInCurrentContext();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private AFAViewModel()
        {
            string homedir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            _excelTemplate = homedir + "\\" + "template.afa.xlsm";
            _wordtemplate = homedir + "\\" + "template.afa.docx";
            _eh = null;
        }

        public string ExcelTemplate
        {
            get
            {
                return _excelTemplate;
            }
            set
            {
                _excelTemplate = value;
            }
        }

        public string WordTemplate
        {
            get
            {
                return _wordtemplate;
            }
            set
            {
                _wordtemplate = value;
            }
        }

        public IAFAErrorHandler ErrorHandler
        {
            get
            {
                return _eh;
            }
            set
            {
                _eh = value;
            }
        }

        public static IAFAViewModel GetInstance()
        {
            if (_instance == null)
                _instance = new AFAViewModel();
            return _instance;
        }

        public bool NewAFAFile(string afaFilename)
        {
            try
            {
                File.Delete(afaFilename);
                File.Copy(ExcelTemplate, afaFilename);
            }
            catch (Exception e)
            {
                if (ErrorHandler != null)
                    return ErrorHandler.HandleException(e);
                return false;
            }
            return true;
        }

        public bool ImportCurrentYear(string afaFilename, string tbXlsxFilename)
        {
            try
            {
                AFAImport.WriteCurrentYearAccountsToAFAFile(afaFilename, AFAImport.ImportFromQuickbooksXLSX(tbXlsxFilename));
            }
            catch (Exception e)
            {
                if (ErrorHandler != null)
                    return ErrorHandler.HandleException(e);
                return false;
            }
            return true;
        }

        public bool ImportPreviousYear(string afaFilename, string tbXlsxFilename)
        {
            try
            {
                AFAImport.WritePreviousYearAccountsToAFAFile(afaFilename, AFAImport.ImportFromQuickbooksXLSX(tbXlsxFilename));
            }
            catch (Exception e)
            {
                if (ErrorHandler != null)
                    return ErrorHandler.HandleException(e);
                return false;
            }
            return true;
        }

        public bool CarryForward(string afaFilename)
        {
            try
            {
                AFARender.CarryForward(afaFilename);
            }
            catch (Exception e)
            {
                if (ErrorHandler != null)
                    return ErrorHandler.HandleException(e);
                return false;
            }
            return true;
        }

        public bool ExportGifi(string afaFilename, string gifiFilename)
        {
            try
            {
                AFARender.CreateGifiFile(gifiFilename, afaFilename);
            }
            catch (Exception e)
            {
                if (ErrorHandler != null)
                    return ErrorHandler.HandleException(e);
                return false;
            }
            return true;
        }

        public bool CreateFinancialStatements(string afaFilename, string fsFilename)
        {
            try
            {
                afaFilename = NormalizeFilename(afaFilename);
                fsFilename = NormalizeFilename(fsFilename);
                AFARender.FillTemplateFromAFAFile(afaFilename, WordTemplate, fsFilename);
            }
            catch (Exception e)
            {
                if (ErrorHandler != null)
                    return ErrorHandler.HandleException(e);
                return false;
            }
            ClearZombieCOMObjects();
            return true;
        }

        private string NormalizeFilename(string s)
        {
            if (!Path.IsPathRooted(s))
                s = Directory.GetCurrentDirectory() + "\\" + s;
            return s;
        }

        public bool Cleanup()
        {
            return true;
        }
        public bool UpdateWord(string afaFilename, string fsFilename)
        {
            AFARender.SetGIFIFields(afaFilename, fsFilename);
            ClearZombieCOMObjects();
            return true;
        }
    }
}
