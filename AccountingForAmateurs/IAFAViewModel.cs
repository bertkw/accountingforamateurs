﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AFA
{
    public interface IAFAErrorHandler
    {
        bool HandleException(Exception e);
    }

    public interface IAFAViewModel
    {
        bool NewAFAFile(string afaFilename);
        bool ImportCurrentYear(string afaFilename, string tbXlsxFilename);
        bool ImportPreviousYear(string afaFilename, string tbXlsxFilename);
        bool CarryForward(string afaFilename);
        bool ExportGifi(string afaFilename, string gifiFilename);
        bool CreateFinancialStatements(string afaFilename, string fsFilename);
        bool Cleanup();
        bool UpdateWord(string afaFilename, string fsFilename);

        IAFAErrorHandler ErrorHandler
        {
            get;
            set;
        }

        string ExcelTemplate
        {
            get;
            set;
        }
        string WordTemplate
        {
            get;
            set;
        }
    }
}
