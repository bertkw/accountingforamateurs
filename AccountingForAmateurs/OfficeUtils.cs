﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Windows;
using System.Diagnostics;

//using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace AccountingForAmateurs.OfficeUtils
{

    /// <summary>
    /// Helper class to decode HTML from the clipboard.
    /// See http://blogs.msdn.com/jmstall/archive/2007/01/21/html-clipboard.aspx for details.
    /// </summary>
    public class HtmlFragment
    {
        #region Read and decode from clipboard
        /// <summary>
        /// Get a HTML fragment from the clipboard.
        /// </summary>    
        /// <example>
        ///    string html = "<b>Hello!</b>";
        ///    HtmlFragment.CopyToClipboard(html);
        ///    HtmlFragment html2 = HtmlFragment.FromClipboard();
        ///    Debug.Assert(html2.Fragment == html);
        /// </example>
        static public HtmlFragment FromClipboard()
        {
            string rawClipboardText = Clipboard.GetText(TextDataFormat.Html);
            HtmlFragment h = new HtmlFragment(rawClipboardText);
            return h;
        }

        /// <summary>
        /// Create an HTML fragment decoder around raw HTML text from the clipboard. 
        /// This text should have the header.
        /// </summary>
        /// <param name="rawClipboardText">raw html text, with header.</param>
        public HtmlFragment(string rawClipboardText)
        {
            // This decodes CF_HTML, which is an entirely text format using UTF-8.
            // Format of this header is described at:
            // http://msdn.microsoft.com/library/default.asp?url=/workshop/networking/clipboard/htmlclipboard.asp

            // Note the counters are byte counts in the original string, which may be Ansi. So byte counts
            // may be the same as character counts (since sizeof(char) == 1).
            // But System.String is unicode, and so byte couns are no longer the same as character counts,
            // (since sizeof(wchar) == 2). 
            int startHMTL = 0;
            int endHTML = 0;

            int startFragment = 0;
            int endFragment = 0;

            Regex r;
            Match m;

            r = new Regex("([a-zA-Z]+):(.+?)[\r\n]",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);

            for (m = r.Match(rawClipboardText); m.Success; m = m.NextMatch())
            {
                string key = m.Groups[1].Value.ToLower();
                string val = m.Groups[2].Value;

                switch (key)
                {
                    // Version number of the clipboard. Starting version is 0.9. 
                    case "version":
                        m_version = val;
                        break;

                    // Byte count from the beginning of the clipboard to the start of the context, or -1 if no context
                    case "starthtml":
                        if (startHMTL != 0) throw new FormatException("StartHtml is already declared");
                        startHMTL = int.Parse(val);
                        break;

                    // Byte count from the beginning of the clipboard to the end of the context, or -1 if no context.
                    case "endhtml":
                        if (startHMTL == 0) throw new FormatException("StartHTML must be declared before endHTML");
                        endHTML = int.Parse(val);

                        m_fullText = rawClipboardText.Substring(startHMTL, endHTML - startHMTL);
                        break;

                    //  Byte count from the beginning of the clipboard to the start of the fragment.
                    case "startfragment":
                        if (startFragment != 0) throw new FormatException("StartFragment is already declared");
                        startFragment = int.Parse(val);
                        break;

                    // Byte count from the beginning of the clipboard to the end of the fragment.
                    case "endfragment":
                        if (startFragment == 0) throw new FormatException("StartFragment must be declared before EndFragment");
                        endFragment = int.Parse(val);
                        m_fragment = rawClipboardText.Substring(startFragment, endFragment - startFragment);
                        break;

                    // Optional Source URL, used for resolving relative links.
                    case "sourceurl":
                        m_source = new System.Uri(val);
                        break;
                }
            } // end for

            if (m_fullText == null && m_fragment == null)
            {
                throw new FormatException("No data specified");
            }
        }


        // Data. See properties for descriptions.
        string m_version;
        string m_fullText;
        string m_fragment;
        System.Uri m_source;

        /// <summary>
        /// Get the Version of the html. Usually something like "1.0".
        /// </summary>
        public string Version
        {
            get { return m_version; }
        }


        /// <summary>
        /// Get the full text (context) of the HTML fragment. This includes tags that the HTML is enclosed in.
        /// May be null if context is not specified.
        /// </summary>
        public string Context
        {
            get { return m_fullText; }
        }


        /// <summary>
        /// Get just the fragment of HTML text.
        /// </summary>
        public string Fragment
        {
            get { return m_fragment; }
        }


        /// <summary>
        /// Get the Source URL of the HTML. May be null if no SourceUrl is specified. This is useful for resolving relative urls.
        /// </summary>
        public System.Uri SourceUrl
        {
            get { return m_source; }
        }

        #endregion // Read and decode from clipboard

        #region Write to Clipboard
        // Helper to convert an integer into an 8 digit string.
        // String must be 8 characters, because it will be used to replace an 8 character string within a larger string.    
        static string To8DigitString(int x)
        {
            return String.Format("{0,8}", x);
        }

        /// <summary>
        /// Clears clipboard and copy a HTML fragment to the clipboard. This generates the header.
        /// </summary>
        /// <param name="htmlFragment">A html fragment.</param>
        /// <example>
        ///    HtmlFragment.CopyToClipboard("<b>Hello!</b>");
        /// </example>
        public static void CopyToClipboard(string htmlFragment)
        {
            CopyToClipboard(htmlFragment, null, null);
        }


        /// <summary>
        /// Clears clipboard and copy a HTML fragment to the clipboard, providing additional meta-information.
        /// </summary>
        /// <param name="htmlFragment">a html fragment</param>
        /// <param name="title">optional title of the HTML document (can be null)</param>
        /// <param name="sourceUrl">optional Source URL of the HTML document, for resolving relative links (can be null)</param>
        public static void CopyToClipboard(string htmlFragment, string title, Uri sourceUrl)
        {
            if (title == null) title = "From Clipboard";

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // Builds the CF_HTML header. See format specification here:
            // http://msdn.microsoft.com/library/default.asp?url=/workshop/networking/clipboard/htmlclipboard.asp

            // The string contains index references to other spots in the string, so we need placeholders so we can compute the offsets. 
            // The <<<<<<<_ strings are just placeholders. We'll backpatch them actual values afterwards.
            // The string layout (<<<) also ensures that it can't appear in the body of the html because the <
            // character must be escaped.
            string header =
    @"Format:HTML Format
Version:1.0
StartHTML:<<<<<<<1
EndHTML:<<<<<<<2
StartFragment:<<<<<<<3
EndFragment:<<<<<<<4
StartSelection:<<<<<<<3
EndSelection:<<<<<<<3
";

            string pre =
    @"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">
<HTML><HEAD><TITLE>" + title + @"</TITLE></HEAD><BODY><!--StartFragment-->";

            string post = @"<!--EndFragment--></BODY></HTML>";

            sb.Append(header);
            if (sourceUrl != null)
            {
                sb.AppendFormat("SourceURL:{0}", sourceUrl);
            }
            int startHTML = sb.Length;

            sb.Append(pre);
            int fragmentStart = sb.Length;

            sb.Append(htmlFragment);
            int fragmentEnd = sb.Length;

            sb.Append(post);
            int endHTML = sb.Length;

            // Backpatch offsets
            sb.Replace("<<<<<<<1", To8DigitString(startHTML));
            sb.Replace("<<<<<<<2", To8DigitString(endHTML));
            sb.Replace("<<<<<<<3", To8DigitString(fragmentStart));
            sb.Replace("<<<<<<<4", To8DigitString(fragmentEnd));


            // Finally copy to clipboard.
            string data = sb.ToString();
            Clipboard.Clear();
            Clipboard.SetText(data, TextDataFormat.Html);
        }

        #endregion // Write to Clipboard
    } // end of class
    public class AFAReadOnlyExcelFile
    {
        private SpreadsheetDocument _spreadSheet;
        private Sheet _currentSheet;

        private SpreadsheetDocument SpreadSheet
        {
            get
            {
                if (_spreadSheet == null)
                {
                    throw new InvalidOperationException("SpreadSheet is null.");
                }
                return _spreadSheet;
            }
        }
        private Sheet CurrentSheet
        {
            get 
            {
                if (_currentSheet == null)
                {
                    _currentSheet = this.SpreadSheet.WorkbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                    if (_currentSheet == null) throw new InvalidOperationException("No Worksheets found.");
                }
                return _currentSheet; 
            }
        }

        public AFAReadOnlyExcelFile(string filename)
        {
            if (!File.Exists(filename)) 
                throw new FileNotFoundException("blargh");
            using (FileStream inStream = File.OpenRead(filename))
            {
                MemoryStream mem = new MemoryStream();
                mem.SetLength(inStream.Length);
                inStream.Read(mem.GetBuffer(), 0, (int)inStream.Length);
                inStream.Close();

                _spreadSheet = SpreadsheetDocument.Open(mem, false);
            }
        }

        ~AFAReadOnlyExcelFile()
        {
            Close();
        }
        private IDictionary<string, Cell> _cellDict;
        public bool SelectSheet(string sheetName)
        {
            Sheet nextSheet = this.SpreadSheet.WorkbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetName).FirstOrDefault();
            
            if (nextSheet == null)
                return false;
            
            _currentSheet = nextSheet;
            _cellDict = null;
            return true;
        }

        public string GetValue(string cellName)
        {
            // Code gratuitously ripped from http://msdn.microsoft.com/en-us/library/hh298534.aspx
            string value = null;
            
            WorkbookPart wbPart = this.SpreadSheet.WorkbookPart;
            WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(CurrentSheet.Id));
            if (_cellDict == null)
            {
                IEnumerable<Cell> cells = wsPart.Worksheet.Descendants<Cell>();
                _cellDict = new Dictionary<string, Cell>(cells.Count<Cell>());
                foreach (Cell c in cells)
                {
                    _cellDict.Add(c.CellReference, c);
                }
            }
            Cell theCell = null;
            if (_cellDict.ContainsKey(cellName))
                theCell = _cellDict[cellName];
            
            if (theCell != null && theCell.CellValue != null)
            {
                value = theCell.CellValue.InnerText;
                
                if (theCell.DataType != null)
                {
                    switch (theCell.DataType.Value)
                    {
                        case CellValues.SharedString:
                            // For shared strings, look up the value in the
                            // shared strings table.
                            var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                            // If the shared string table is missing, something 
                            // is wrong. Return the index that is in
                            // the cell. Otherwise, look up the correct text in 
                            // the table.
                            if (stringTable != null)
                            {
                                value = stringTable.SharedStringTable.ElementAt(int.Parse(value)).InnerText;
                            }
                            break;

                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "FALSE";
                                    break;
                                default:
                                    value = "TRUE";
                                    break;
                            }
                            break;
                    }
                }
            }
            return value;
        }

        public void Close()
        {
            if (_spreadSheet != null)
            {
                _spreadSheet.Dispose();
                _spreadSheet = null;
            }
        }
    }
    public class AFAExcelFile
    {
        private SpreadsheetDocument _spreadSheet;
        private Sheet _currentSheet;

        private SpreadsheetDocument SpreadSheet
        {
            get
            {
                if (_spreadSheet == null)
                {
                    throw new InvalidOperationException("SpreadSheet is null.");
                }
                return _spreadSheet;
            }
        }
        private Sheet CurrentSheet
        {
            get
            {
                if (_currentSheet == null)
                {
                    _currentSheet = this.SpreadSheet.WorkbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();
                    if (_currentSheet == null) throw new InvalidOperationException("No Worksheets found.");
                }
                return _currentSheet;
            }
        }

        public AFAExcelFile(string filename)
        {
            _spreadSheet = SpreadsheetDocument.Open(filename, true);

        }

        ~AFAExcelFile()
        {
            Close();
        }

        public bool SelectSheet(string sheetName)
        {
            Sheet nextSheet = this.SpreadSheet.WorkbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetName).FirstOrDefault();

            if (nextSheet == null)
                return false;

            _currentSheet = nextSheet;
            return true;
        }

        public string GetValue(string cellName)
        {
            // Code gratuitously ripped from http://msdn.microsoft.com/en-us/library/hh298534.aspx
            string value = null;

            WorkbookPart wbPart = this.SpreadSheet.WorkbookPart;
            WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(CurrentSheet.Id));

            Cell theCell = wsPart.Worksheet.Descendants<Cell>().Where(c => c.CellReference == cellName).FirstOrDefault();
            if (theCell != null && theCell.CellValue != null)
            {
                value = theCell.CellValue.InnerText;

                if (theCell.DataType != null)
                {
                    switch (theCell.DataType.Value)
                    {
                        case CellValues.SharedString:
                            // For shared strings, look up the value in the
                            // shared strings table.
                            var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                            // If the shared string table is missing, something 
                            // is wrong. Return the index that is in
                            // the cell. Otherwise, look up the correct text in 
                            // the table.
                            if (stringTable != null)
                            {
                                value = stringTable.SharedStringTable.ElementAt(int.Parse(value)).InnerText;
                            }
                            break;

                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "FALSE";
                                    break;
                                default:
                                    value = "TRUE";
                                    break;
                            }
                            break;
                    }
                }
            }
            return value;
        }
        public void SetFormula(string columnName, int rowIndex, string formula)
        {
            WorkbookPart wbPart = this.SpreadSheet.WorkbookPart;
            WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(CurrentSheet.Id));

            Cell c = InsertCellInWorksheet(columnName, (uint)rowIndex, wsPart);
            if (c.CellFormula == null)
            {
                if (c.CellValue != null)
                    c.CellValue.Remove();
                CellFormula cf = new CellFormula();
                c.Append(cf);
            }
            c.CellFormula.Text = formula;
        }
        public void SetValue(string columnName, int rowIndex, double value)
        {
            WorkbookPart wbPart = this.SpreadSheet.WorkbookPart;
            WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(CurrentSheet.Id));

            Cell c = InsertCellInWorksheet(columnName, (uint)rowIndex, wsPart);
            c.CellValue = new CellValue(value.ToString());
            c.DataType = new EnumValue<CellValues>(CellValues.Number);
        }
        public void SetValue(string columnName, int rowIndex, int value)
        {
            WorkbookPart wbPart = this.SpreadSheet.WorkbookPart;
            WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(CurrentSheet.Id));

            Cell c = InsertCellInWorksheet(columnName, (uint)rowIndex, wsPart);
            c.CellValue = new CellValue(value.ToString());
            c.DataType = new EnumValue<CellValues>(CellValues.Number);
        }

        public void SetValue(string columnName, int rowIndex, string text)
        {
            WorkbookPart wbPart = this.SpreadSheet.WorkbookPart;
            WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(CurrentSheet.Id));
            
            SharedStringTablePart shareStringPart;
            if (this.SpreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().Count() > 0)
            {
                shareStringPart = this.SpreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
            }
            else
            {
                shareStringPart = this.SpreadSheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
            }
            
            // Insert the text into the SharedStringTablePart.
            int index = InsertSharedStringItem(text, shareStringPart);

            Cell c = InsertCellInWorksheet(columnName, (uint)rowIndex, wsPart);
            if (text == null)
                c.Remove();
            else
            {
                c.CellValue = new CellValue(index.ToString());
                c.DataType = new EnumValue<CellValues>(CellValues.SharedString);
            }
        }
        // Given a column name, a row index, and a WorksheetPart, inserts a cell into the worksheet. 
        // If the cell already exists, returns it. 
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
        // Given text and a SharedStringTablePart, creates a SharedStringItem with the specified text 
        // and inserts it into the SharedStringTablePart. If the item already exists, returns its index.
        private static int InsertSharedStringItem(string text, SharedStringTablePart shareStringPart)
        {
            // If the part does not contain a SharedStringTable, create one.
            if (shareStringPart.SharedStringTable == null)
            {
                shareStringPart.SharedStringTable = new SharedStringTable();
            }

            int i = 0;

            // Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
            foreach (SharedStringItem item in shareStringPart.SharedStringTable.Elements<SharedStringItem>())
            {
                if (item.InnerText == text)
                {
                    return i;
                }

                i++;
            }

            // The text does not exist in the part. Create the SharedStringItem and return its index.
            shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
            shareStringPart.SharedStringTable.Save();

            return i;
        }
        public void Save()
        {
            WorkbookPart wbPart = this.SpreadSheet.WorkbookPart;
            WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(CurrentSheet.Id));

            wbPart.Workbook.CalculationProperties.ForceFullCalculation = true;
            wbPart.Workbook.CalculationProperties.FullCalculationOnLoad = true;

            wsPart.Worksheet.Save();
        }
        public void Close()
        {
            if (_spreadSheet != null)
            {
                _spreadSheet.Dispose();
                _spreadSheet = null;
            }
        }
    }
    public class OfficeCOMCleanupHelper
    {
        public static List<object> Documents = new List<object>();
        public static List<object> Applications = new List<object>();
        public static void Cleanup()
        {
            foreach (object o in Documents)
                try
                { Marshal.FinalReleaseComObject(o); }
                catch { }
            foreach (object o in Applications)
                try
                { Marshal.FinalReleaseComObject(o); }
                catch { }
        }
    }
}
