﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using AccountingForAmateurs.OfficeUtils;
using System.IO;
using System.Windows.Automation;
using System.Windows.Forms;
using System.Diagnostics;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;


namespace AccountingForAmateurs.Model
{
    public class GIFIAccount
    {
        int _code, _cy, _py;
        string _name, _type;
        public static string IntFormatHelper(int i)
        {
            bool neg = false;
            if (i == 0)
                return "- ";
            if (i < 0)
            {
                neg = true;
                i *= -1;
            }
            string s = i.ToString("#,##0");
            if (neg)
                s = "(" + s + ")";
            else
                s = s + " ";
            return "   " + s; 
        }
        public GIFIAccount(int code, string name, string cy, string py, string type)
        {
            _code = code;
            try
            {
                _cy = int.Parse(cy, NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign);
            }
            catch 
            {
                _cy = 0;
                Console.Error.WriteLine(cy);
            }
            try
            {
                _py = int.Parse(py, NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign);
            }
            catch 
            {
                _py = 0;
                Console.Error.WriteLine(py);
            }
            _name = name;
            _type = type;
        }
        public int Code { get { return _code; } }
        public string CurrentYear 
        { 
            get 
            {
                return IntFormatHelper(_cy);
            } 
        }
        public string PreviousYear 
        { 
            get 
            {
                return IntFormatHelper(_py);
            } 
        }
        public int CurrentYearAsInt
        {
            get
            {
                return _cy;
            }
        }
        public int PreviousYearAsInt
        {
            get
            {
                return _py;
            }
        }
        public string Name { get { return _name; } }
        public string Type { get { return _type; } }
    }
    public class Account
    {
        int _number;
        double _value;
        string _name;
        public Account(int number = 0, string name = null, double value = 0.0)
        {
            _number = number;
            _name = name;
            _value = value;
        }
        public int AccountNumber
        {
            get { return _number; }
        }
        public string Name
        {
            get { return _name; }
        }
        public double Value
        {
            get { return _value; }
        }
    }
    public class AFARenderData
    {
        private List<GIFIAccount> _gifi;
        private Dictionary<string, string> _bookmarks;
        private Dictionary<string, string> _settings;
        public List<GIFIAccount> GIFIAccounts { get { return _gifi; } }
        public Dictionary<string, string> Bookmarks { get { return _bookmarks; } }
        public Dictionary<string, string> Settings { get { return _settings; } }
        public AFARenderData()
        {
            _gifi = new List<GIFIAccount>(200);
            _bookmarks = new Dictionary<string, string>();
            _settings = new Dictionary<string, string>();
        }
    }

    public class AFAImport
    {
        public static List<Account> ImportFromQuickbooksXLSX(string filename)
        {
            AFAReadOnlyExcelFile tb = new AFAReadOnlyExcelFile(filename);
            
            int row = 3;
            tb.SelectSheet("Sheet1");
            List<Account> list = new List<Account>(256);
            Regex rx1 = new Regex(@"^(\d\d\d\d) ·");
            Regex rx2 = new Regex(@":(\d\d\d\d) ·");
            while (true)
            {
                string name = null;
                double value;
                int i = -1;
                
                name = tb.GetValue("B" + row);
                if (name == null || name.Length == 0)
                    break;
                
                string s = tb.GetValue("C" + row);
                if (s != null && s.Length > 0)
                {
                    value = double.Parse(s);
                }
                else
                {
                    s = tb.GetValue("E" + row);
                    if (s == null || s.Length == 0)
                        throw new ArgumentException("Error in file: " + filename + ", row " + row + ".");
                    value = -1 * double.Parse(s);
                }
               
                Match m = rx1.Match(name);
                if (m.Groups.Count > 1) i = int.Parse(m.Groups[1].ToString());
                MatchCollection m2 = rx2.Matches(name);
                if (m2.Count > 0)
                {
                    m = m2[m2.Count - 1];
                    if (m.Groups.Count > 1) i = int.Parse(m.Groups[1].ToString());
                }
                
                list.Add(new Account(i, name, value));
                row++;
            }
            return list;
        }
        public static void WriteAccountsToAFAFile(string afaFilename, string worksheetName, List<Account> accounts)
        {
            AFAExcelFile afaFile = new AFAExcelFile(afaFilename);
            afaFile.SelectSheet(worksheetName);
            for (int i = 2; i < 200; i++)
            {
                string s = afaFile.GetValue("E" + i);
                if (s != null && s.Length > 0)
                {
                    afaFile.SetValue("A", i, null);
                    afaFile.SetValue("C", i, null);
                    afaFile.SetValue("E", i, null);
                }
            }
            for (int i = 0; i < accounts.Count; i++)
            {
                Account acc = accounts[i];
                afaFile.SetValue("A", i + 2, acc.AccountNumber);
                afaFile.SetValue("C", i + 2, acc.Name);
                afaFile.SetValue("E", i + 2, acc.Value);
            }
            afaFile.Save();
            afaFile.Close();
        }
        public static void WriteCurrentYearAccountsToAFAFile(string afaFilename, List<Account> accounts)
        {
            WriteAccountsToAFAFile(afaFilename, "CurrentYear", accounts);
        }
        public static void WritePreviousYearAccountsToAFAFile(string afaFilename, List<Account> accounts)
        {
            WriteAccountsToAFAFile(afaFilename, "PreviousYear", accounts);
        }

        
    }
    
    public class AFARender
    {
        class HTMLRowHelper
        {
            public string[] TextArray;
            public string[] ClassArray;
            int _columns;
            public HTMLRowHelper(int columns)
            {
                TextArray = new string[columns];
                ClassArray = new string[columns];
                _columns = columns;
            }
            public string Render()
            {
                StringBuilder sb = new StringBuilder(512);
                sb.Append("<TR>");
                if (_columns == 5)
                {
                    for (int i = 0; i < _columns; i++)
                    {
                        string align = "ALIGN='RIGHT'";
                        if (i == 0) align = "";
                        sb.Append("<TD " + align + " CLASS='" + ClassArray[i] + "'>" + TextArray[i] + "</TD>");
                    }
                }
                sb.Append("</TR>");
                return sb.ToString();
            }
        }
        private static string AddDollarSign(string s)
        {
            return "<table width='100%' border=0 cellpadding=0 cellspacing=0><tr><td align=left width=0>$</td><td align=right>" + s + "</td></tr></table>";
        }
        private static Word.Cell AddDollarSign(Word.Cell parent)
        {
            Word.Table t = parent.Range.Document.Tables.Add(parent.Range, 1, 2);
            t.LeftPadding = 0;
            t.RightPadding = 0;
            t.TopPadding = 0;
            t.BottomPadding = 0;
            
            Word.Column col1 = t.Columns[1];
            col1.SetWidth((float)11.0F, Word.WdRulerStyle.wdAdjustNone);
            Word.Column col2 = t.Columns[2];
            col2.SetWidth(parent.Width - col1.Width, Word.WdRulerStyle.wdAdjustNone);
            Word.Cell c = t.Cell(1, 1);
            c.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
            t.Cell(1, 1).Range.Text = "$";
            
            return t.Cell(1,2);
        }
        private static void UnderlineRow(Word.Row row)
        {
            row.Cells[3].Borders[Word.WdBorderType.wdBorderBottom].LineStyle = Word.WdLineStyle.wdLineStyleSingle;
            row.Cells[5].Borders[Word.WdBorderType.wdBorderBottom].LineStyle = Word.WdLineStyle.wdLineStyleSingle;
        }
        private static void DoubleUnderlineRow(Word.Row row)
        {
            row.Cells[3].Borders[Word.WdBorderType.wdBorderBottom].LineStyle = Word.WdLineStyle.wdLineStyleDouble;
            row.Cells[5].Borders[Word.WdBorderType.wdBorderBottom].LineStyle = Word.WdLineStyle.wdLineStyleDouble;
        }
        private static string GIFI_FIELD_NAME(GIFIAccount gifi, string prefix)
        {
            return prefix + "_GIFI_" + gifi.Code;
        }
        private static string GIFI_FIELD_NAME_CY(GIFIAccount gifi)
        {
            return GIFI_FIELD_NAME(gifi, "CY");
        }
        private static string GIFI_FIELD_NAME_PY(GIFIAccount gifi)
        {
            return GIFI_FIELD_NAME(gifi, "PY");
        }
        private static void LinkCellToGIFI_CY(Word.Document doc, Word.Cell cell, GIFIAccount gifi)
        {
            Word.Range r = cell.Range;
            r.Collapse();
            r.Select();
            Word.Field f = doc.Fields.Add(doc.Application.ActiveWindow.Selection.Range, Word.WdFieldType.wdFieldRef, GIFI_FIELD_NAME_CY(gifi));
        }
        private static void LinkCellToGIFI_PY(Word.Document doc, Word.Cell cell, GIFIAccount gifi)
        {
            Word.Range r = cell.Range;
            r.Collapse();
            r.Select();
            Word.Field f = doc.Fields.Add(doc.Application.ActiveWindow.Selection.Range, Word.WdFieldType.wdFieldRef, GIFI_FIELD_NAME_PY(gifi));
        }
        private static void FillRowFromGIFI(Word.Document doc, Word.Row row, GIFIAccount gifi, ref bool first)
        {
            if (first)
            {
                FillRowFromGIFIWithDollarSigns(doc, row, gifi);
                first = false;
                return;
            }
            LinkCellToGIFI_CY(doc, row.Cells[3], gifi);
            LinkCellToGIFI_PY(doc, row.Cells[5], gifi);
        }
        private static void FillRowFromGIFIWithDollarSigns(Word.Document doc, Word.Row row, GIFIAccount gifi)
        {
            Word.Cell wc = AddDollarSign(row.Cells[3]);
            LinkCellToGIFI_CY(doc, wc, gifi);
            wc = AddDollarSign(row.Cells[5]);
            LinkCellToGIFI_PY(doc, wc, gifi);
        }
        private static string CAPITALASSETS_SPECIALACC_FIELDNAME_CY()
        {
            return "CY_CAPITALASSETS_SPECIAL";
        }
        private static string CAPITALASSETS_SPECIALACC_FIELDVAL_CY(AFARenderData rd)
        {
            GIFIAccount[] totalcapitalassets = rd.GIFIAccounts.Where(x => x.Type.Equals("CX")).ToArray();
            return totalcapitalassets[0].CurrentYear;
        }
        private static string CAPITALASSETS_SPECIALACC_FIELDNAME_PY()
        {
            return "PY_CAPITALASSETS_SPECIAL";
        }
        private static string CAPITALASSETS_SPECIALACC_FIELDVAL_PY(AFARenderData rd)
        {
            GIFIAccount[] totalcapitalassets = rd.GIFIAccounts.Where(x => x.Type.Equals("CX")).ToArray();
            return totalcapitalassets[0].PreviousYear;
        }

        private static string OTHERASSETS_SPECIALACC_FIELDNAME_CY()
        {
            return "CY_OTHERASSETS_SPECIAL";
        }
        private static string OTHERASSETS_SPECIALACC_FIELDVAL_CY(AFARenderData rd)
        {
            GIFIAccount[] totalotherassets = rd.GIFIAccounts.Where(x => x.Type.Equals("DX")).ToArray();
            return totalotherassets[0].CurrentYear;
        }
        private static string OTHERASSETS_SPECIALACC_FIELDNAME_PY()
        {
            return "PY_OTHERASSETS_SPECIAL";
        }
        private static string OTHERASSETS_SPECIALACC_FIELDVAL_PY(AFARenderData rd)
        {
            GIFIAccount[] totalotherassets = rd.GIFIAccounts.Where(x => x.Type.Equals("DX")).ToArray();
            return totalotherassets[0].PreviousYear;
        }
        public static void RenderBalanceSheet(Word.Document doc, AFARenderData rd)
        {
            Word.Bookmark bk = null;
            char blank = ' ';
            Word.Cell c = null;
            Word.Row r = null;
            Word.Column col = null;
            Word.Table t = null;
            HashSet<Word.Row> underlineList = new HashSet<Word.Row>();
            HashSet<Word.Row> dblUnderlineList = new HashSet<Word.Row>();
            HashSet<Word.Row> boldList = new HashSet<Word.Row>();

            
            bk = find_bookmark(doc, "BALANCESHEET_MARK");
            t = doc.Tables.Add(bk.Range, 2, 5);
            
            t.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleNone;
            t.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleNone;
            t.LeftPadding = 0;
            t.RightPadding = 0;
            t.TopPadding = 0;
            t.BottomPadding = 0;

            t.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitFixed);
            /*
             * 6.65 inches = 100% of page
             * pp1 = points in one percent of the page
             */
            decimal pp1 = (decimal)doc.Application.InchesToPoints(6.65F) / 100;
            float minWidth = 11.0F;
            col = t.Columns[2];
            col.SetWidth((float)minWidth, Word.WdRulerStyle.wdAdjustNone);

            col = t.Columns[3];
            col.SetWidth((float)pp1 * 16, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 16.0F;

            col = t.Columns[4];
            col.SetWidth((float)minWidth, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 1.0F;
            
            col = t.Columns[5];
            col.SetWidth((float)pp1 * 16, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 16.0F;

            col = t.Columns[1];
            col.SetWidth((float)pp1 * 61, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 66.0F;

            c = t.Cell(2, 3);
            c.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
            c.Range.Text = rd.Settings["Current Year"] + blank;
            c = t.Cell(2, 5);
            c.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
            c.Range.Text = rd.Settings["Previous Year"] + blank;
            t.Rows.Add();
            r = t.Rows.Add();
            c = r.Cells[1];
            c.Range.Text = "ASSETS";
            boldList.Add(r);
            #region Assets
            GIFIAccount[] currentassets = rd.GIFIAccounts.Where(x => x.Type.StartsWith("A") && (x.CurrentYearAsInt != 0 || x.PreviousYearAsInt != 0)).ToArray();
            GIFIAccount[] longtermassets = rd.GIFIAccounts.Where(x => x.Type.StartsWith("B") && (x.CurrentYearAsInt != 0 || x.PreviousYearAsInt != 0)).ToArray();
            GIFIAccount[] totalcapitalassets = rd.GIFIAccounts.Where(x => x.Type.Equals("CX")).ToArray();
            GIFIAccount[] totalotherassets = rd.GIFIAccounts.Where(x => x.Type.Equals("DX")).ToArray();
            GIFIAccount[] totalassets = rd.GIFIAccounts.Where(x => x.Type.Equals("TA")).ToArray();

            Word.Row r_CurrentAssetsBottom = null;
            Word.Row r_LongTermAssetsBottom = null;
            Word.Row r_TotalCapitalAssets = null;
            Word.Row r_TotalOtherAssets = null;
            Word.Row dblunderline_next = null;

            int count = 0;
            bool first = true;

            if (currentassets.Length > 0)
            {
                count++;
                r = t.Rows.Add();
                r.Range.Font.Bold = 0;
                c = r.Cells[1];
                c.Range.Text = "Current";
                if (currentassets.Length == 2)
                {
                    r = t.Rows.Add();
                    r.Cells[1].Range.Text = "    " + currentassets[0].Name;
                    FillRowFromGIFI(doc, r, currentassets[0], ref first);
                    r_CurrentAssetsBottom = r;
                    dblunderline_next = r;
                }
                else if (currentassets.Length > 2)
                {
                    for (int i = 0; i < currentassets.Length - 1; i++)
                    {
                        r = t.Rows.Add();
                        r.Cells[1].Range.Text = "    " + currentassets[i].Name;
                        FillRowFromGIFI(doc, r, currentassets[i], ref first);
                    }
                    underlineList.Add(r);
                    r = t.Rows.Add();
                    FillRowFromGIFI(doc, r, currentassets[currentassets.Length - 1], ref first);
                    r_CurrentAssetsBottom = r;
                    dblunderline_next = r;
                }
            }
            if (longtermassets.Length == 2)
            {
                count++;
                t.Rows.Add();
                r = t.Rows.Add();
                r.Cells[1].Range.Text = longtermassets[0].Name;
                FillRowFromGIFI(doc, r, longtermassets[0], ref first);
                r_LongTermAssetsBottom = r;
                dblunderline_next = r;
            }
            if (longtermassets.Length > 2)
            {
                count++;
                r = t.Rows.Add();
                r.Cells[1].Range.Text = "Long-term";
                for (int i = 0; i < longtermassets.Length - 1; i++)
                {
                    r.Cells[1].Range.Text = "    " + longtermassets[i].Name;
                    FillRowFromGIFI(doc, r, longtermassets[i], ref first);
                }
                underlineList.Add(r);
                r = t.Rows.Add();

                r.Cells[1].Range.Text = longtermassets[longtermassets.Length - 1].Name;
                FillRowFromGIFI(doc, r, longtermassets[longtermassets.Length - 1], ref first);
                r_LongTermAssetsBottom = r;
                dblunderline_next = r;
            }
            if (totalcapitalassets.Length > 0 && (totalcapitalassets[0].CurrentYearAsInt > 0 || totalcapitalassets[0].PreviousYearAsInt > 0))
            {
                count++;
                t.Rows.Add();
                r = t.Rows.Add();

                r.Cells[1].Range.Text = totalcapitalassets[0].Name;
                Word.Range rng = null;
                c = r.Cells[3];
                rng = c.Range;
                rng.Collapse();
                rng.Select();
                Word.Field f = doc.Fields.Add(doc.Application.ActiveWindow.Selection.Range, Word.WdFieldType.wdFieldRef, CAPITALASSETS_SPECIALACC_FIELDNAME_CY());
                c = r.Cells[5];
                rng = c.Range;
                rng.Collapse();
                rng.Select();
                f = doc.Fields.Add(doc.Application.ActiveWindow.Selection.Range, Word.WdFieldType.wdFieldRef, CAPITALASSETS_SPECIALACC_FIELDNAME_PY());

                r_TotalCapitalAssets = r;
                dblunderline_next = r;
            }
            if (totalotherassets.Length > 0 && (totalotherassets[0].CurrentYearAsInt > 0 || totalotherassets[0].PreviousYearAsInt > 0))
            {
                count++;
                t.Rows.Add();
                r = t.Rows.Add();

                r.Cells[1].Range.Text = totalotherassets[0].Name;
                Word.Range rng = null;
                c = r.Cells[3];
                rng = c.Range;
                rng.Collapse();
                rng.Select();
                Word.Field f = doc.Fields.Add(doc.Application.ActiveWindow.Selection.Range, Word.WdFieldType.wdFieldRef, OTHERASSETS_SPECIALACC_FIELDNAME_CY());
                c = r.Cells[5];
                rng = c.Range;
                rng.Collapse();
                rng.Select();
                f = doc.Fields.Add(doc.Application.ActiveWindow.Selection.Range, Word.WdFieldType.wdFieldRef, OTHERASSETS_SPECIALACC_FIELDNAME_PY());


                r_TotalOtherAssets = r;
                dblunderline_next = r;
            }
            if (count == 1)
            {
                FillRowFromGIFIWithDollarSigns(doc, r, currentassets[currentassets.Length - 1]);
            }
            if (count > 1)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = totalassets[0].Name;
                FillRowFromGIFIWithDollarSigns(doc, r, totalassets[0]);
                dblunderline_next = r;

                if (r_TotalOtherAssets != null)
                    underlineList.Add(r_TotalOtherAssets);
                else if (r_TotalCapitalAssets != null)
                    underlineList.Add(r_TotalCapitalAssets);
                else if (r_LongTermAssetsBottom != null)
                    underlineList.Add(r_LongTermAssetsBottom);

                if (r_CurrentAssetsBottom != null && r_LongTermAssetsBottom != null)
                {
                    underlineList.Add(r_CurrentAssetsBottom);
                    underlineList.Add(r_LongTermAssetsBottom);
                }
            }
            t.Rows.Add();
            #endregion
            #region LIABILITIES AND EQUITY
            GIFIAccount[] currentliabilities = rd.GIFIAccounts.Where(x => x.Type.StartsWith("E") && (x.CurrentYearAsInt > 0 || x.PreviousYearAsInt > 0)).ToArray();
            GIFIAccount[] longtermliabilities = rd.GIFIAccounts.Where(x => x.Type.StartsWith("F") && (x.CurrentYearAsInt > 0 || x.PreviousYearAsInt > 0)).ToArray();
            GIFIAccount[] totalliabilities = rd.GIFIAccounts.Where(x => x.Type.Equals("TL")).ToArray();
            GIFIAccount[] shareholdersequity = rd.GIFIAccounts.Where(x => x.Type.StartsWith("G")).ToArray();
            GIFIAccount[] totalliabilitiesequity = rd.GIFIAccounts.Where(x => x.Type.Equals("TLE")).ToArray();
            Word.Row lastLiability = null;
            r = t.Rows.Add();
            c = r.Cells[1];
            c.Range.Text = "LIABILITIES";
            boldList.Add(r);
            
            count = 0;
            first = true;
            Word.Row lastCurrentLiability = null;
            Word.Row lastLongtermLiability = null;
            if (currentliabilities.Length == 2)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = "Current";
                
                r = t.Rows.Add();
                r.Cells[1].Range.Text = "    " + currentliabilities[0].Name;
                FillRowFromGIFI(doc, r, currentliabilities[0], ref first);
                count++;
                lastLiability = r;
                lastCurrentLiability = r;
                underlineList.Add(r);
            }
            if (currentliabilities.Length > 2)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = "Current";

                for (int i = 0; i < currentliabilities.Length - 1; i++)
                {
                    r = t.Rows.Add();
                    r.Cells[1].Range.Text = "    " + currentliabilities[i].Name;
                    FillRowFromGIFI(doc, r, currentliabilities[i], ref first);
                }
                underlineList.Add(r);
                r = t.Rows.Add();
                FillRowFromGIFI(doc, r, currentliabilities[currentliabilities.Length - 1], ref first);
                lastLiability = r;
                lastCurrentLiability = r;
                count++;
            }

            if (longtermliabilities.Length > 0)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = "Long-term";
                for (int i = 0; i < longtermliabilities.Length - 1; i++)
                {
                    r = t.Rows.Add();
                    r.Cells[1].Range.Text = "    " + longtermliabilities[i].Name;
                    FillRowFromGIFI(doc, r, longtermliabilities[i], ref first);
                }
                underlineList.Add(r);

                if (longtermliabilities.Length > 2)
                {
                    r = t.Rows.Add();
                    r.Cells[1].Range.Text = longtermliabilities[longtermliabilities.Length - 1].Name;
                    FillRowFromGIFI(doc, r, longtermliabilities[longtermliabilities.Length - 1], ref first);
                }
                lastLiability = r;
                lastLongtermLiability = r;
                count++;
            }
            underlineList.Add(lastLiability);
            if (count > 1)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = totalliabilities[0].Name;
                FillRowFromGIFI(doc, r, totalliabilities[0], ref first);
                underlineList.Add(r);
                underlineList.Add(lastCurrentLiability);
                underlineList.Add(lastLongtermLiability);
            }

            r = t.Rows.Add();
            string equitytitle = rd.Settings["shareholder possessive"].ToUpper() + " " + rd.Settings["equity_this_year"].ToUpper();
            r = t.Rows.Add();
            c = r.Cells[1];
            c.Range.Text = equitytitle;
            boldList.Add(r);
            for (int i = 0; i < shareholdersequity.Length - 1; i++)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = "    " + shareholdersequity[i].Name;
                FillRowFromGIFI(doc, r, shareholdersequity[i], ref first);

                if (i == shareholdersequity.Length - 2)
                {
                    underlineList.Add(r);
                }
            }
            r = t.Rows.Add();
            FillRowFromGIFI(doc, r, shareholdersequity[shareholdersequity.Length - 1], ref first);
            underlineList.Add(r);

            r = t.Rows.Add();
            FillRowFromGIFIWithDollarSigns(doc, r, totalliabilitiesequity[0]);
            dblUnderlineList.Add(r);
            #endregion
            if (dblunderline_next != null)
            {
                dblUnderlineList.Add(dblunderline_next);
                dblunderline_next = null;
            }

            t.Range.Font.Bold = 0; 
            foreach (Word.Row row in underlineList)
                UnderlineRow(row);
            foreach (Word.Row row in dblUnderlineList)
                DoubleUnderlineRow(row);
            foreach (Word.Row row in boldList)
                row.Range.Font.Bold = 1;
            t.Rows[2].Borders[Word.WdBorderType.wdBorderBottom].LineStyle = Word.WdLineStyle.wdLineStyleDouble;

            if (rd.Settings.ContainsKey("Hide comparative") && rd.Settings["Hide comparative"].ToLower() == "yes")
            {
                float adj = t.Columns[4].Width + t.Columns[5].Width;
                t.Columns[5].Delete();
                t.Columns[4].Delete();
                t.Columns[1].Width += adj;
            }
        }
        private static string RETAINED_SPECIALACC_FIELDNAME_CY()
        {
            return "CY_RETAINED_SPECIAL";
        }
        private static string RETAINED_SPECIALACC_FIELDVAL_CY(AFARenderData rd)
        {
            GIFIAccount[] retainedearnings = rd.GIFIAccounts.Where(x => x.Type.StartsWith("H")).ToArray();
            GIFIAccount beginning = retainedearnings[0];
            GIFIAccount income = retainedearnings[1];
            GIFIAccount dividends = retainedearnings[2];
            GIFIAccount ending = retainedearnings[3];
            return GIFIAccount.IntFormatHelper(income.CurrentYearAsInt + beginning.CurrentYearAsInt);
        }
        private static string RETAINED_SPECIALACC_FIELDNAME_PY()
        {
            return "PY_RETAINED_SPECIAL";
        }
        private static string RETAINED_SPECIALACC_FIELDVAL_PY(AFARenderData rd)
        {
            GIFIAccount[] retainedearnings = rd.GIFIAccounts.Where(x => x.Type.StartsWith("H")).ToArray();
            GIFIAccount beginning = retainedearnings[0];
            GIFIAccount income = retainedearnings[1];
            GIFIAccount dividends = retainedearnings[2];
            GIFIAccount ending = retainedearnings[3];
            return GIFIAccount.IntFormatHelper(income.PreviousYearAsInt + beginning.PreviousYearAsInt);
        }
        public static void RenderRetainedEarnings(Word.Document doc, AFARenderData rd)
        {
            Word.Bookmark bk = null;
            char blank = ' ';
            Word.Cell c = null;
            Word.Row r = null;
            Word.Column col = null;
            Word.Table t = null;
            HashSet<Word.Row> underlineList = new HashSet<Word.Row>();
            HashSet<Word.Row> dblUnderlineList = new HashSet<Word.Row>();
            HashSet<Word.Row> boldList = new HashSet<Word.Row>();

            Word.Range rng = null;
            bk = find_bookmark(doc, "RETAINED_MARK");
            t = doc.Tables.Add(bk.Range, 2, 5);
            
            t.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleNone;
            t.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleNone;
            t.LeftPadding = 0;
            t.RightPadding = 0;
            t.TopPadding = 0;
            t.BottomPadding = 0;

            t.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitFixed);
            /*
             * 6.65 inches = 100% of page
             * pp1 = points in one percent of the page
             */
            decimal pp1 = (decimal)doc.Application.InchesToPoints(6.65F) / 100;
            float minWidth = 11.0F;
            col = t.Columns[2];
            col.SetWidth((float)minWidth, Word.WdRulerStyle.wdAdjustNone);

            col = t.Columns[3];
            col.SetWidth((float)pp1 * 16, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 16.0F;

            col = t.Columns[4];
            col.SetWidth((float)minWidth, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 1.0F;

            col = t.Columns[5];
            col.SetWidth((float)pp1 * 16, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 16.0F;

            col = t.Columns[1];
            col.SetWidth((float)pp1 * 61, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 66.0F;

            c = t.Cell(2, 3);
            c.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
            c.Range.Text = rd.Settings["Current Year"] + blank;
            c = t.Cell(2, 5);
            c.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
            c.Range.Text = rd.Settings["Previous Year"] + blank;
            t.Rows.Add();


            bool first = true;
            GIFIAccount[] retainedearnings = rd.GIFIAccounts.Where(x => x.Type.StartsWith("H")).ToArray();
            GIFIAccount beginning = retainedearnings[0];
            GIFIAccount income = retainedearnings[1];
            GIFIAccount dividends = retainedearnings[2];
            GIFIAccount ending = retainedearnings[3];

            r = t.Rows.Add();
            r.Cells[1].Range.Text = beginning.Name;
            FillRowFromGIFI(doc, r, beginning, ref first);
            t.Rows.Add();

            r = t.Rows.Add();
            r.Cells[1].Range.Text = income.Name;
            FillRowFromGIFI(doc, r, income, ref first);
            underlineList.Add(r);

            r = t.Rows.Add();
            c = r.Cells[3];
            rng = c.Range;
            rng.Collapse();
            rng.Select();
            Word.Field f = doc.Fields.Add(doc.Application.ActiveWindow.Selection.Range, Word.WdFieldType.wdFieldRef, RETAINED_SPECIALACC_FIELDNAME_CY());
            c = r.Cells[5];
            rng = c.Range;
            rng.Collapse();
            rng.Select();
            f = doc.Fields.Add(doc.Application.ActiveWindow.Selection.Range, Word.WdFieldType.wdFieldRef, RETAINED_SPECIALACC_FIELDNAME_PY());
            t.Rows.Add();

            r = t.Rows.Add();
            r.Cells[1].Range.Text = "    " + dividends.Name;
            FillRowFromGIFI(doc, r, dividends, ref first);
            underlineList.Add(r);
            
            // Extra line between bottom line and final ending amouns
            r = t.Rows.Add();

            r = t.Rows.Add();
            r.Cells[1].Range.Text = ending.Name;
            FillRowFromGIFIWithDollarSigns(doc, r, ending);
            dblUnderlineList.Add(r);

            t.Range.Font.Bold = 0; 
            foreach (Word.Row row in underlineList)
                UnderlineRow(row);
            foreach (Word.Row row in dblUnderlineList)
                DoubleUnderlineRow(row);
            foreach (Word.Row row in boldList)
                row.Range.Font.Bold = 1;
            t.Rows[2].Borders[Word.WdBorderType.wdBorderBottom].LineStyle = Word.WdLineStyle.wdLineStyleDouble;
            
            if (rd.Settings.ContainsKey("Hide comparative") && rd.Settings["Hide comparative"].ToLower() == "yes")
            {
                float adj = t.Columns[4].Width + t.Columns[5].Width;
                t.Columns[5].Delete();
                t.Columns[4].Delete();
                t.Columns[1].Width += adj;
            }

        }

        private static string OTHERINCOME_SPECIALACC_FIELDNAME_CY()
        {
            return "CY_OTHERINCOME_SPECIAL";
        }
        private static string OTHERINCOME_SPECIALACC_FIELDVAL_CY(AFARenderData rd)
        {
            GIFIAccount[] otherincome = rd.GIFIAccounts.Where(x => x.Type.StartsWith("K") && (x.CurrentYearAsInt != 0 || x.PreviousYearAsInt != 0)).ToArray();
            if (otherincome.Length == 0)
                return GIFIAccount.IntFormatHelper(0);
            return otherincome[otherincome.Length - 1].CurrentYear;
        }
        private static string OTHERINCOME_SPECIALACC_FIELDNAME_PY()
        {
            return "PY_OTHERINCOME_SPECIAL";
        }
        private static string OTHERINCOME_SPECIALACC_FIELDVAL_PY(AFARenderData rd)
        {
            GIFIAccount[] otherincome = rd.GIFIAccounts.Where(x => x.Type.StartsWith("K") && (x.CurrentYearAsInt != 0 || x.PreviousYearAsInt != 0)).ToArray();
            if (otherincome.Length == 0)
                return GIFIAccount.IntFormatHelper(0);
            return otherincome[otherincome.Length - 1].PreviousYear;
        }
        public static void RenderIncomeStatement(Word.Document doc, AFARenderData rd)
        {
            Word.Bookmark bk = null;
            char blank = ' ';
            Word.Cell c = null;
            Word.Row r = null;
            Word.Column col = null;
            Word.Table t = null;
            List<Word.Row> underlineList = new List<Word.Row>();
            List<Word.Row> dblUnderlineList = new List<Word.Row>();
            List<Word.Row> boldList = new List<Word.Row>();
            
            bk = find_bookmark(doc, "INCOME_MARK");
            t = doc.Tables.Add(bk.Range, 2, 5);

            t.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleNone;
            t.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleNone;
            t.LeftPadding = 0;
            t.RightPadding = 0;
            t.TopPadding = 0;
            t.BottomPadding = 0;

            t.AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitFixed);
            /*
             * 6.65 inches = 100% of page
             * pp1 = points in one percent of the page
             */
            decimal pp1 = (decimal)doc.Application.InchesToPoints(6.65F) / 100;
            float minWidth = 11.0F;
            col = t.Columns[2];
            col.SetWidth((float)minWidth, Word.WdRulerStyle.wdAdjustNone);

            col = t.Columns[3];
            col.SetWidth((float)pp1 * 16, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 16.0F;

            col = t.Columns[4];
            col.SetWidth((float)minWidth, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 1.0F;

            col = t.Columns[5];
            col.SetWidth((float)pp1 * 16, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 16.0F;

            col = t.Columns[1];
            col.SetWidth((float)pp1 * 61, Word.WdRulerStyle.wdAdjustNone);
            //col.Width = 66.0F;

            c = t.Cell(2, 3);
            c.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
            c.Range.Text = rd.Settings["Current Year"] + blank;
            c = t.Cell(2, 5);
            c.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
            c.Range.Text = rd.Settings["Previous Year"] + blank;
            t.Rows.Add();

            bool first = true;

            GIFIAccount[] sales = rd.GIFIAccounts.Where(x => x.Type.Equals("IX")).ToArray();
            GIFIAccount[] costofsales = rd.GIFIAccounts.Where(x => x.Type.Equals("JX")).ToArray();
            GIFIAccount[] grossmargin = rd.GIFIAccounts.Where(x => x.Type.Equals("O")).ToArray();
            GIFIAccount[] otherincome = rd.GIFIAccounts.Where(x => x.Type.StartsWith("K") && (x.CurrentYearAsInt != 0 || x.PreviousYearAsInt != 0)).ToArray();
            GIFIAccount[] expenses = rd.GIFIAccounts.Where(x => x.Type.StartsWith("L") && (x.CurrentYearAsInt != 0 || x.PreviousYearAsInt != 0)).ToArray();
            GIFIAccount[] taxes = rd.GIFIAccounts.Where(x => x.Type.StartsWith("M")).ToArray();
            GIFIAccount[] netincome = rd.GIFIAccounts.Where(x => x.Type.Equals("NI")).ToArray();

            r = t.Rows.Add();
            r.Cells[1].Range.Text = sales[0].Name;
            FillRowFromGIFI(doc, r, sales[0], ref first);

            if (rd.Settings.ContainsKey("Show Sales Only") && rd.Settings["Show Sales Only"].ToLower() == "yes")
            {
                underlineList.Add(r);
            }
            else
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = costofsales[0].Name;
                FillRowFromGIFI(doc, r, costofsales[0], ref first);
                underlineList.Add(r);

                r = t.Rows.Add();
                r.Cells[1].Range.Text = grossmargin[0].Name;
                FillRowFromGIFI(doc, r, grossmargin[0], ref first);
                underlineList.Add(r);
            }
            t.Rows.Add();

            if (otherincome.Length == 2)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = "Other income";

                r = t.Rows.Add();
                r.Cells[1].Range.Text = "    " + otherincome[0].Name;
                FillRowFromGIFI(doc, r, otherincome[0], ref first);
                underlineList.Add(r);

                t.Rows.Add();
            }
            if (otherincome.Length > 2)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = "Other income";

                for (int i = 0; i < otherincome.Length - 1; i++)
                {
                    r = t.Rows.Add();
                    r.Cells[1].Range.Text = "    " + otherincome[i].Name;
                    FillRowFromGIFI(doc, r, otherincome[i], ref first);
                    if (i == otherincome.Length - 2)
                    {
                        underlineList.Add(r);
                    }
                }
                r = t.Rows.Add();
                r.Cells[1].Range.Text = otherincome[otherincome.Length - 1].Name;
                Word.Range rng = null;
                c = r.Cells[3];
                rng = c.Range;
                rng.Collapse();
                rng.Select();
                Word.Field f = doc.Fields.Add(doc.Application.ActiveWindow.Selection.Range, Word.WdFieldType.wdFieldRef, OTHERINCOME_SPECIALACC_FIELDNAME_CY());
                c = r.Cells[5];
                rng = c.Range;
                rng.Collapse();
                rng.Select();
                f = doc.Fields.Add(doc.Application.ActiveWindow.Selection.Range, Word.WdFieldType.wdFieldRef, OTHERINCOME_SPECIALACC_FIELDNAME_PY());

                underlineList.Add(r);

                t.Rows.Add();    
            }

            if (expenses.Length == 2)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = "Operating expenses";

                r = t.Rows.Add();
                r.Cells[1].Range.Text = "    " + expenses[0].Name;
                FillRowFromGIFI(doc, r, expenses[0], ref first);
                underlineList.Add(r);
            }
            if (expenses.Length > 2)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = "Operating expenses";

                for (int i = 0; i < expenses.Length - 1; i++)
                {
                    r = t.Rows.Add();
                    r.Cells[1].Range.Text = "    " + expenses[i].Name;
                    FillRowFromGIFI(doc, r, expenses[i], ref first);
                    if (i == expenses.Length - 2)
                    {
                        underlineList.Add(r);
                    }
                }
                r = t.Rows.Add();
                FillRowFromGIFI(doc, r, expenses[expenses.Length - 1], ref first);
                underlineList.Add(r);
            }

            t.Rows.Add();

            if (taxes[1].CurrentYearAsInt != 0 || taxes[1].PreviousYearAsInt != 0)
            {
                r = t.Rows.Add();
                r.Cells[1].Range.Text = taxes[0].Name;
                FillRowFromGIFI(doc, r, taxes[0], ref first);

                r = t.Rows.Add();
                r.Cells[1].Range.Text = taxes[1].Name;
                FillRowFromGIFI(doc, r, taxes[1], ref first);
                underlineList.Add(r);
            }

            r = t.Rows.Add();

            r.Cells[1].Range.Text = netincome[0].Name;
            FillRowFromGIFIWithDollarSigns(doc, r, netincome[0]);
            dblUnderlineList.Add(r);

            t.Range.Font.Bold = 0; 
            foreach (Word.Row row in underlineList)
                UnderlineRow(row);
            foreach (Word.Row row in dblUnderlineList)
                DoubleUnderlineRow(row);
            foreach (Word.Row row in boldList)
                row.Range.Font.Bold = 1;
            t.Rows[2].Borders[Word.WdBorderType.wdBorderBottom].LineStyle = Word.WdLineStyle.wdLineStyleDouble;

            if (rd.Settings.ContainsKey("Hide comparative") && rd.Settings["Hide comparative"].ToLower() == "yes")
            {
                float adj = t.Columns[4].Width + t.Columns[5].Width;
                t.Columns[5].Delete();
                t.Columns[4].Delete();
                t.Columns[1].Width += adj;
            }
        }

        public static AFARenderData ReadRenderDataFromAFAFile(string filename)
        {
            AFAReadOnlyExcelFile afaFile = new AFAReadOnlyExcelFile(filename);
            AFARenderData rd = new AFARenderData();
            
            afaFile.SelectSheet("GIFI");
            string type = null;
            int row = 3;
            while (true)
            {
                type = afaFile.GetValue("B" + row);
                if (type == "END")
                    break;
                if (type == null || type.Length == 0)
                {
                    row++;
                    continue;
                }

                int code = int.Parse(afaFile.GetValue("A" + row), NumberStyles.AllowThousands);
                string cy = afaFile.GetValue("E" + row);
                string py = afaFile.GetValue("G" + row);
                string name = afaFile.GetValue("C" + row);
                rd.GIFIAccounts.Add(new GIFIAccount(code, name, cy, py, type));

                row++;
            }

            afaFile.SelectSheet("Settings");
            row = 1;
            while (true)
            {
                string name = afaFile.GetValue("A" + row);
                if (name == "END")
                    break;
                if (name == null || name.Length == 0)
                {
                    row++;
                    continue;
                }
                string value = afaFile.GetValue("C" + row);

                rd.Settings.Add(name, value);
                
                row++;
            }

            row = 1;
            afaFile.SelectSheet("WORDBOOKMARKS");
            while (true)
            {
                string name = afaFile.GetValue("A" + row);
                if (name == "END")
                    break;
                if (name == null || name.Length == 0)
                {
                    row++;
                    continue;
                }

                string value = afaFile.GetValue("C" + row);
                rd.Bookmarks.Add(name, value);

                row++;
            }
            afaFile.Close();
            return rd;
        }

        static Word.Bookmark find_bookmark(Word.Document doc, string s)
        {

            foreach (Word.Bookmark bk in doc.Bookmarks)
            {
                if (bk.Name.Equals(s)) return bk;
            }
            return null;
        }
        public static void SetGIFIFields(string afaFilename, string fsFilename)
        {
            AFARenderData rd = AFARender.ReadRenderDataFromAFAFile(afaFilename);
            Word.Application word = null;
            Word.Document doc = null;
            try
            {
                word = new Word.Application();
                word.Visible = false;

                word.Options.Pagination = false;
                word.ScreenUpdating = false;

                object missing = Type.Missing;
                doc = null;
                object ofalse = false;
                object name = fsFilename;
                doc = word.Documents.Open(ref name, ref ofalse, false, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                doc.Activate();
                doc.ActiveWindow.View.Type = Word.WdViewType.wdNormalView;
                SetGIFIFields(doc, rd);
                doc.Save();
                doc.Close();
                word.Application.Quit();
                word.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(doc);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(word);
            }
            catch (IOException e)
            {
                if (doc != null) doc.Close();
                if (word != null)
                {
                    word.Application.Quit();
                    word.Quit();
                }
            }
        }
        public static void SetGIFIFields(Word.Document doc, AFARenderData rd)
        {
            Word.Range fieldsRange = null;
            foreach (Word.Table table in doc.Tables)
            {
                if (table.Rows.Count == 1 && table.Columns.Count == 1)
                {
                    Word.Cell c = table.Cell(1, 1);
                    fieldsRange = c.Range;
                    fieldsRange.Delete();
                    fieldsRange.Collapse();
                    fieldsRange.Select();
                    fieldsRange = doc.Application.ActiveWindow.Selection.Range;
                    break;
                }
            }

            Word.Field f = null;
            foreach (GIFIAccount gifi in rd.GIFIAccounts)
            {
                string key_cy = GIFI_FIELD_NAME_CY(gifi);
                string val_cy = gifi.CurrentYear;
                string key_py = GIFI_FIELD_NAME_PY(gifi);
                string val_py = gifi.PreviousYear;

                f = doc.Fields.Add(fieldsRange, Word.WdFieldType.wdFieldSet, key_cy + " \"" + val_cy + "\"", true);
                f.ShowCodes = true;
                f = doc.Fields.Add(fieldsRange, Word.WdFieldType.wdFieldSet, key_py + " \"" + val_py + "\"", true);
                f.ShowCodes = true;
            }
            f = doc.Fields.Add(fieldsRange, Word.WdFieldType.wdFieldSet, RETAINED_SPECIALACC_FIELDNAME_CY() + " \"" + RETAINED_SPECIALACC_FIELDVAL_CY(rd) + "\"", true);
            f.ShowCodes = true;
            f = doc.Fields.Add(fieldsRange, Word.WdFieldType.wdFieldSet, RETAINED_SPECIALACC_FIELDNAME_PY() + " \"" + RETAINED_SPECIALACC_FIELDVAL_PY(rd) + "\"", true);
            f.ShowCodes = true;

            f = doc.Fields.Add(fieldsRange, Word.WdFieldType.wdFieldSet, CAPITALASSETS_SPECIALACC_FIELDNAME_CY() + " \"" + CAPITALASSETS_SPECIALACC_FIELDVAL_CY(rd) + "\"", true);
            f.ShowCodes = true;
            f = doc.Fields.Add(fieldsRange, Word.WdFieldType.wdFieldSet, CAPITALASSETS_SPECIALACC_FIELDNAME_PY() + " \"" + CAPITALASSETS_SPECIALACC_FIELDVAL_PY(rd) + "\"", true);
            f.ShowCodes = true;

            f = doc.Fields.Add(fieldsRange, Word.WdFieldType.wdFieldSet, OTHERASSETS_SPECIALACC_FIELDNAME_CY() + " \"" + OTHERASSETS_SPECIALACC_FIELDVAL_CY(rd) + "\"", true);
            f.ShowCodes = true;
            f = doc.Fields.Add(fieldsRange, Word.WdFieldType.wdFieldSet, OTHERASSETS_SPECIALACC_FIELDNAME_PY() + " \"" + OTHERASSETS_SPECIALACC_FIELDVAL_PY(rd) + "\"", true);
            f.ShowCodes = true;

            f = doc.Fields.Add(fieldsRange, Word.WdFieldType.wdFieldSet, OTHERINCOME_SPECIALACC_FIELDNAME_CY() + " \"" + OTHERINCOME_SPECIALACC_FIELDVAL_CY(rd) + "\"", true);
            f.ShowCodes = true;
            f = doc.Fields.Add(fieldsRange, Word.WdFieldType.wdFieldSet, OTHERINCOME_SPECIALACC_FIELDNAME_PY() + " \"" + OTHERINCOME_SPECIALACC_FIELDVAL_PY(rd) + "\"", true);
            f.ShowCodes = true;
        }
        public static void FillTemplateFromAFAFile(string afaFilename, string templateFilename, string targetFilename)
        {
            AFARenderData rd = AFARender.ReadRenderDataFromAFAFile(afaFilename);
            File.Delete(targetFilename);
            File.Copy(templateFilename, targetFilename);
            string temppath = Path.GetTempPath();
            Excel.Application excel = null;
            Excel.Workbook xls = null;
            Word.Application word = null;
            Word.Document doc = null;
            try
            {
                object missing = Type.Missing;
                excel = new Excel.Application();
                word = new Word.Application();
                word.Visible = false;
                
                word.Options.Pagination = false;
                word.ScreenUpdating = false;

                excel.DisplayAlerts = false;
                excel.ScreenUpdating = false;
                excel.Visible = false;
                excel.UserControl = false;
                excel.Interactive = false;

                xls = excel.Workbooks.Open(afaFilename, true, missing, missing,
                                    missing, missing, missing, missing, missing, missing, missing, missing,
                                    missing, missing, true);
                doc = null;
                object ofalse = false;
                object name = targetFilename;
                doc = word.Documents.Open(ref name, ref ofalse, false, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                doc.Activate();
                doc.ActiveWindow.View.Type = Word.WdViewType.wdNormalView;

                Word.Bookmark bk = null;

                object format = Microsoft.Office.Interop.Excel.XlFileFormat.xlHtml;
                System.Collections.IEnumerator wsEnumerator = excel.ActiveWorkbook.Worksheets.GetEnumerator();
                Microsoft.Office.Interop.Excel.Worksheet wsCurrent = null;
                while (wsEnumerator.MoveNext())
                {
                    wsCurrent = (Microsoft.Office.Interop.Excel.Worksheet)wsEnumerator.Current;
                    if (wsCurrent.Name == "Notes_Display") break;
                }
                string notesHtml = null;
                if (wsCurrent != null)
                {
                    notesHtml = temppath + "\\" + Guid.NewGuid().ToString("N") + ".htm";
                    Excel.Range r = excel.get_Range("NOTES_FOR_PRINTING");
                    r = r.SpecialCells(Excel.XlCellType.xlCellTypeVisible, Type.Missing);
                    r.Copy();
                    Excel.Worksheet newSheet = xls.Worksheets.Add();
                    newSheet.Activate();
                    for (int i = 1; i <= 10; i++)
                    {
                        newSheet.Columns[i].ColumnWidth = wsCurrent.Columns[i].ColumnWidth;
                    }
                    newSheet.Paste();
                    newSheet.Activate();
                    r = excel.Selection;
                    r.Copy();

                    xls.PublishObjects.Add(Microsoft.Office.Interop.Excel.XlSourceType.xlSourceSheet, notesHtml, newSheet.Name, newSheet.Name, Microsoft.Office.Interop.Excel.XlHtmlType.xlHtmlStatic, "dsg", "").Publish(true);

                    RenderBalanceSheet(doc, rd);
                    RenderRetainedEarnings(doc, rd);
                    RenderIncomeStatement(doc, rd);

                    /*
                    object oNotesName = notesHtml;
                    Word.Document doc2 = word.Documents.Open(ref oNotesName, ref ofalse, false, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                    object start = doc2.Content.Start;
                    object end = doc2.Content.End;

                    Word.Range notesrng = doc2.Range(ref start, ref end);
                    notesrng.Copy();
                    doc2.Close();
                     */
                }
                bk = find_bookmark(doc, "NOTES_MARK");
                if (bk == null)
                    Console.WriteLine("bookmark not found!");
                else
                    bk.Range.InsertFile(notesHtml);
                Word.Range rng = null;

                rng = bk.Range;
                File.Delete(notesHtml);
                
                object what = Word.WdGoToItem.wdGoToLine;
                object which = Word.WdGoToDirection.wdGoToLast;

                rng = word.ActiveWindow.Selection.GoTo(ref what, ref which, ref missing, ref missing);
                rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

                foreach (string key in rd.Bookmarks.Keys)
                {
                    string val = rd.Bookmarks[key];
                    Word.Field f = doc.Fields.Add(rng, Word.WdFieldType.wdFieldSet, key + " \"" + val + "\"", false);
                    f.ShowCodes = true;
                    f.Update();
                }
                SetGIFIFields(doc, rd);
                doc.Fields.Update();
                foreach (Word.Section wordSection in doc.Sections)
                {
                    foreach (Word.HeaderFooter hf in wordSection.Headers)
                    {
                        hf.Range.Fields.Update();
                    }
                } 
                //MessageBox.Show(timesSB.ToString());
            }
            catch (IOException e)
            {
                if (doc != null) doc.Close();
                if (word != null)
                {
                    word.Application.Quit();
                    word.Quit();
                }
                if (xls != null) xls.Close();
                if (excel != null)
                {
                    excel.Application.Quit();
                    excel.Quit();
                }
                throw (e);
            }
            xls.Close();
            excel.Application.Quit();
            excel.Quit();

            doc.Save();
            doc.Close();
            word.Application.Quit();
            word.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xls);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excel);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(doc);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(word);
            
            xls = null;
            excel = null;
            word = null;
            doc = null;
        }

        public static void CreateGifiFile(string gifiFilename, string afaFilename)
        {
            AFARenderData rd = AFARender.ReadRenderDataFromAFAFile(afaFilename);
            File.Delete(gifiFilename);

            using (TextWriter tx = new StreamWriter(gifiFilename))
            {
                int[] requiredIDs = { 2599, 3499, 3620, 8299, 9368, 3849, 9999 };
                Dictionary<int, int> existingAccounts = new Dictionary<int, int>(rd.GIFIAccounts.Count + requiredIDs.Length);

                string firstline = "\"GIFI01\",\"0001\",\"ASCII\",\"-\",\"L\",\"\",\"\",\"\",\"\",\"\",\"\",\"5\",\"2\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"";
                tx.WriteLine(firstline);

                foreach (GIFIAccount acc in rd.GIFIAccounts)
                    if (acc.Code != 0 && acc.CurrentYearAsInt != 0) existingAccounts[acc.Code] = acc.CurrentYearAsInt;
                foreach (int i in requiredIDs)
                    if (!existingAccounts.ContainsKey(i)) existingAccounts[i] = 0;

                IEnumerable<int> orderedAccountIDs = existingAccounts.Keys.OrderBy(x => x);
                foreach (int id in orderedAccountIDs)
                {
                    int amt = existingAccounts[id];
                    tx.WriteLine("\"" + id + "\"," + amt);
                }
                tx.WriteLine("\"GIFI02\",9999");
            }
        }
        public static void CarryForward(string afaFilename)
        {
            AFAExcelFile afaFile = new AFAExcelFile(afaFilename);
            
            afaFile.SelectSheet("PreviousYear");
            for (int i = 2; i < 300; i++)
            {
                string s = afaFile.GetValue("E" + i);
                if (s != null && s.Length > 0)
                {
                    afaFile.SetValue("A", i, null);
                    afaFile.SetValue("C", i, null);
                    afaFile.SetValue("E", i, null);
                    afaFile.SetValue("H", i, null);
                }
            }
            afaFile.SelectSheet("CurrentYear");
            for (int i = 2; i < 300; i++)
            {
                string A, C, E, H;
                E = afaFile.GetValue("E" + i);
                if (E != null && E.Length > 0)
                {
                    A = afaFile.GetValue("A" + i);
                    C = afaFile.GetValue("C" + i);
                    E = afaFile.GetValue("E" + i);
                    H = afaFile.GetValue("H" + i);
                    if (H != null && (H == "" || H.Trim().Length == 0))
                    {
                        H = null;
                    }

                    afaFile.SelectSheet("PreviousYear");
                    afaFile.SetValue("A", i, int.Parse(A));
                    afaFile.SetValue("C", i, C);
                    afaFile.SetValue("E", i, Double.Parse(E));
                    if (H != null)
                    {
                        int hValue;
                        bool b = int.TryParse(H, out hValue);
                        if (b)
                            afaFile.SetValue("H", i, hValue);
                    }
                    afaFile.SelectSheet("CurrentYear");
                }
            }
            afaFile.SelectSheet("CurrentYear");
            for (int i = 2; i < 300; i++)
            {
                string s = afaFile.GetValue("E" + i);
                if (s != null && s.Length > 0)
                {
                    afaFile.SetValue("A", i, null);
                    afaFile.SetValue("C", i, null);
                    afaFile.SetValue("E", i, null);
                    afaFile.SetValue("H", i, null);
                }
            }

            Update(afaFile);

            afaFile.Save();
            afaFile.Close();
        }

        public static void Update(AFAExcelFile afaFile)
        {
            afaFile.SelectSheet("Settings");
            string s = afaFile.GetValue("C18");
            if (s == null)
            {
                afaFile.SetValue("A", 18, "AFA File Version");
                afaFile.SetValue("C", 18, 1.01);

                afaFile.SetFormula("C", 23, "=IF(OR(C10=\"Yes\",RETAINEDEARNINGS_PY = 0),S_RETAINED_CY,IF(OR(F23=G23,RETAINEDEARNINGS_CY=0),G23,IF(RETAINEDEARNINGS_CY>=0,\"Retained earnings (deficit)\",\"(Deficit) retained earnings\")))");
                afaFile.SetFormula("C", 24, "=IF(OR(C10=\"Yes\",INCOME_PY = 0),S_INCOME_CY,IF(OR(F24=G24,INCOME_CY=0),G24,IF(INCOME_CY>=0,\"Income (loss)\",\"(Loss) income\")))");
                afaFile.SetFormula("C", 25, "=IF(OR(C10=\"Yes\",EQUITY_PY = 0),F25,IF(OR(F25=G25,EQUITY_CY=0),G25,IF(EQUITY_CY>=0,\"Equity (deficiency)\",\"(Deficiency) equity\")))");
            }
        }
    }

}
