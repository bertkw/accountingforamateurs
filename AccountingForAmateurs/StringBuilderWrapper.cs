﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace AFA
{
    public class StringBuilderWrapper : INotifyPropertyChanged
    {
        private StringBuilder _sb;
        private String _propname;

        public string PropertyName
        {
            get { return _propname; }
            set { _propname = value; }
        }
        public string Text
        {
            get { return _sb.ToString(); }
        }
        
        public StringBuilderWrapper()
        {
            _sb = new StringBuilder(1024);
            _propname = PropertyName;
        }
        public void AppendLine(string s)
        {
            _sb.AppendLine(s);
            OnPropertyChanged();
        }
        public void Append(string s)
        {
            _sb.Append(s);
            OnPropertyChanged();
        }
        public void Clear()
        {
            _sb.Clear();
            OnPropertyChanged();
        }
        protected void OnPropertyChanged()
        {
            var handler = PropertyChanged;
            if (handler != null && _propname != null)
            { handler(this, new PropertyChangedEventArgs(_propname)); }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
