﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using AccountingForAmateurs.Model;

namespace AccountingForAmateurs.ViewModels
{
    public class AFAViewModel : INotifyPropertyChanged
    {
        public AFAViewModel()
        {
            
        }

        #region PropertyChanged Block
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
        #endregion
    }
}
